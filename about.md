---
layout: page
title: About
permalink: /about/
---

This is the base Jekyll theme (modified). You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](http://jekyllrb.com/)

You can find the source code for this project at:
<https://gitlab.com/gitlab-tests/jekyll>.